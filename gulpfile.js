'use strict';

var gulp = require('gulp'),
	sass = require('gulp-sass'),
	uglify = require('gulp-uglify'),
	concat = require('gulp-concat'),
	rename = require('gulp-rename'),
	sourcemaps = require('gulp-sourcemaps'),
	cssMinify = require('gulp-minify-css'),
	angularTemplateCache = require('gulp-angular-templatecache'),
	es = require('event-stream'),
	browserSync = require("browser-sync"),
	reload = browserSync.reload;

var dist = 'dist';
var src = 'src';
var test = 'test';
var bc = 'bower_components';

var srcHtml = [
	src + '/index.html',
	src + '/**/*.html'
];

var srcJs = [
	'src/**/*.module.js',
	'src/**/*.config.js',
	'src/**/*.service.js',
	'src/**/*.js'
];

var vendorSrcJs = [
	bc + '/angular/angular.min.js',
	bc + '/angular-ui-router/release/angular-ui-router.min.js',
	bc + '/angular-sanitize/angular-sanitize.min.js',
	bc + '/angular-bootstrap/ui-bootstrap-tpls.min.js',
	bc + '/bootstrap/dist/bootstrap.min.js',
	bc + '/angularUtils-pagination/dirPagination.js',
	bc + '/angular-loading-bar/build/loading-bar.min.js',
	bc + '/angular-animate/angular-animate.min.js'
];
var vendorSrcCSS = [
	bc + '/bootstrap/dist/css/bootstrap.min.css',
	bc + '/angular-loading-bar/build/loading-bar.min.css'
];
var srcFonts = [
	src  + '/assets/fonts/*',
	bc + '/bootstrap/dist/fonts/*'
];

var srcImg = [
	src + '/assets/img/*'
];

gulp.task('default', ['build', 'watch', 'webserver']);
gulp.task('build', ['js', 'sass', 'html', 'vendor', 'fonts', 'img']);

gulp.task('watch', function() {
	gulp.watch(src + '/**/*.js', ['js']);
	gulp.watch(src + '/**/*.scss', ['sass']);
	gulp.watch(src + '/*/**/*.html', ['html']);
	gulp.watch(src + '/index.html', ['html']);
});

gulp.task('sass', function() {
	gulp.src(src + '/**/*.scss')
		.pipe(sourcemaps.init())
		.pipe(sass().on('error', sass.logError))
		.pipe(concat('app.css'))
		.pipe(cssMinify())
		.pipe(rename({
			suffix: '.min'
		}))
		.pipe(sourcemaps.write())
		.pipe(gulp.dest(dist + '/app/'))
		.pipe(reload({stream: true}))
});

gulp.task('js', function() {
	return es.merge(
		gulp.src(srcJs),
		gulp.src(src + '/**/*.html')
			.pipe(angularTemplateCache({
				standalone: true,
				module: 'templates'
			})))
		.pipe(sourcemaps.init())
		.pipe(uglify())
		.pipe(concat('app.js'))
		.pipe(rename({
			suffix: '.min'
		}))

		.pipe(sourcemaps.write())
		.pipe(gulp.dest(dist + '/app'))
		.pipe(reload({stream: true}));

});

gulp.task('html', function() {
	return gulp.src(srcHtml)
		.pipe(gulp.dest(dist))
		.pipe(reload({stream: true}))
});

gulp.task('vendor', function() {
	return es.merge(
		gulp.src(vendorSrcCSS)
			.pipe(concat('vendor.min.css'))
			.pipe(gulp.dest(dist + '/vendor')),
		gulp.src(vendorSrcJs)
			.pipe(concat('vendor.min.js'))
			.pipe(gulp.dest(dist + '/vendor'))
	);
});

gulp.task('fonts', function() {
    gulp.src(srcFonts)
        .pipe(gulp.dest(dist + "/fonts"))
		.pipe(reload({stream: true}))
});

gulp.task("img", function() {
	gulp.src(srcImg)
		.pipe(gulp.dest(dist + "/img"))
});

var config = {
    server: {
        baseDir: "./dist"
    },
    tunnel: true,
    host: 'localhost',
    port: 9000,
    logPrefix: "Frontend_Devil"
};
gulp.task('webserver', function () {
    browserSync(config);
});

// var path = {
//     build: { //Тут мы укажем куда складывать готовые после сборки файлы
//         html: 'build/',
//         js: 'build/js/',
//         css: 'build/css/',
//         img: 'build/img/',
//         fonts: 'build/fonts/'
//     },
//     src: { //Пути откуда брать исходники
//         html: 'src/*.html', //Синтаксис src/*.html говорит gulp что мы хотим взять все файлы с расширением .html
//         js: 'src/js/main.js',//В стилях и скриптах нам понадобятся только main файлы
//         style: 'src/style/main.scss',
//         img: 'src/img/**/*.*', //Синтаксис img/**/*.* означает - взять все файлы всех расширений из папки и из вложенных каталогов
//         fonts: 'src/fonts/**/*.*'
//     },
//     watch: { //Тут мы укажем, за изменением каких файлов мы хотим наблюдать
//         html: 'src/**/*.html',
//         js: 'src/js/**/*.js',
//         style: 'src/style/**/*.scss',
//         img: 'src/img/**/*.*',
//         fonts: 'src/fonts/**/*.*'
//     },
//     clean: './build'
// };



/*gulp.task('html:build', function () {
    gulp.src(path.src.html) //Выберем файлы по нужному пути
        .pipe(rigger()) //Прогоним через rigger
        .pipe(gulp.dest(path.build.html)) //Выплюнем их в папку build
        .pipe(reload({stream: true})); //И перезагрузим наш сервер для обновлений
});

gulp.task('js:build', function () {
    gulp.src(path.src.js) //Найдем наш main файл
        .pipe(rigger()) //Прогоним через rigger
        .pipe(sourcemaps.init()) //Инициализируем sourcemap
        .pipe(uglify()) //Сожмем наш js
        .pipe(sourcemaps.write()) //Пропишем карты
        .pipe(gulp.dest(path.build.js)) //Выплюнем готовый файл в build
        .pipe(reload({stream: true})); //И перезагрузим сервер
});

gulp.task('style:build', function () {
    gulp.src(path.src.style) //Выберем наш main.scss
        .pipe(sourcemaps.init()) //То же самое что и с js
        .pipe(sass()) //Скомпилируем
        .pipe(prefixer()) //Добавим вендорные префиксы
        .pipe(cssmin()) //Сожмем
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(path.build.css)) //И в build
        .pipe(reload({stream: true}));
});

gulp.task('image:build', function () {
    gulp.src(path.src.img) //Выберем наши картинки
        .pipe(imagemin({ //Сожмем их
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            use: [pngquant()],
            interlaced: true
        }))
        .pipe(gulp.dest(path.build.img)) //И бросим в build
        .pipe(reload({stream: true}));
});

gulp.task('fonts:build', function() {
    gulp.src(path.src.fonts)
        .pipe(gulp.dest(path.build.fonts))
});

gulp.task('build', [
    'html:build',
    'js:build',
    'style:build',
    'fonts:build',
    'image:build'
]);*/

/*gulp.task('watch', function(){
    watch([path.watch.html], function(event, cb) {
        gulp.start('html:build');
    });
    watch([path.watch.style], function(event, cb) {
        gulp.start('style:build');
    });
    watch([path.watch.js], function(event, cb) {
        gulp.start('js:build');
    });
    watch([path.watch.img], function(event, cb) {
        gulp.start('image:build');
    });
    watch([path.watch.fonts], function(event, cb) {
        gulp.start('fonts:build');
    });
});*/

//gulp.task('default', ['build', 'webserver', 'watch']);

/*var gulp = require('gulp');
var plugins = require('gulp-load-plugins')();
var bowerFiles = require('main-bower-files');
var browserSync = require('browser-sync');

*/
//var paths = {
//	cripts: '.app/**/*.js',
//	styles: ['.app/scss/**/*.css', './app/scss/**/*.scss'],
//	images: './app/img/**/*',
//	index: './app/index.html',
//	partials : ['./app/**/*.html', '!app/index.html'],
//	distDev: './dist.dev',
//	distProd: './dist.prod'
//};

/* var pipes = {};

pipes.orderedVendorScripts = function() {
	return plugins.order(['angular.js', 'bootstrap.js']);
};

pipes.buildVendorScriptsDev = function() {
	return gulp.src(bowerFiles())
		.pipe(gulp.dest(paths.distDev + '/bower_components'));
};

pipes.buildAppScriptsDev = function() {
	return gulp.src(paths.scripts)
		.pipe(plugins.concat('app.js'))
		.pipe(gulp.dest(paths.distDev));
};

pipes.buildIndexDev = function() {

	var orderedVendorScripts = pipes.buildVendorScriptsDev()
		.pipe(pipes.orderedVendorScripts());

	var orderedAppScripts = pipes.buildAppScriptsDev();

	return gulp.src(paths.index)
		.pipe(gulp.dest(paths.distDev))
		.pipe(plugins.inject(orderedVendorScripts, {relative: true, name: 'bower'}))
		.pipe(plugins.inject(orderedAppScripts, {relative: true }))
		.pipe(gulp.dest(paths.distDev));
};
// task

gulp.task('build', pipes.buildAppScriptsDev);
gulp.task('build-index', pipes.buildIndexDev);
gulp.task('watch-dev', function() {
	var reload = browserSync.reload;

	browserSync({
		port:8000,
		server: {
			baseDir: paths.distDev
		}
	});
	gulp.watch(paths.index, function() {
		return pipes.buildIndexDev()
			.pipe(reload({stream: true}));
	})
});
*/