angular
	.module('championships')
	.controller('ChampionshipsCtrl', ['$scope', '$http',
		function($scope, $http) {
			var championships, teams, i, j;
			$http.get("http://footballbet.com.ua/api/championships/")
	    		.success(function (data) {
	    			$scope.imageChamp = 'http://footballbet.com.ua/table/embl/';
	    			championships = data.result;
		    		$scope.championships = championships;
	    			$scope.orderByFunction = function(item){
					    return parseInt(item.num_order);
					};
			
				$http.get("http://footballbet.com.ua/api/teams/")
		    		.success(function (data) {
		    			$scope.imageTeam = 'http://footballbet.com.ua/teams/embl/';
		    			teams = data.result;
		    			var selectedTeams = [];
		    			$scope.getTeams = function (id, name, sename) {
			    				for(j=0; j < teams.length; j++) {
									if(id === teams[j].id_championship) {
										selectedTeams.push(teams[j]);		
									}
								$scope.champSename = sename;
								$scope.champName = name;
								}
							$scope.teams = selectedTeams;
							$scope.showTable = true;
							$scope.sortType     = 'name';
							$scope.sortReverse  = false;
							$scope.searchItem   = ''; 
			    		}
		    	});

	    	});
	    		
		}
]);






















/*			$scope.navCollapsed = true;

			$scope.links = [
				{name: 'ssTest.menu.groups', state: '.groups', checkArr: [
					'groups', 'addGroup', 'editGroup', 'students', 'registerUser'
				]},
				{name: 'ssTest.menu.subjects', state: '.subjects', checkArr: [
					'subjects', 'addSubject', 'editSubject',
					'tests', 'addTest', 'editTest', 'schedule',
					'questions', 'addQuestion', 'editQuestion',
					'detailOfTest', 'addTestDetail', 'editDetailOfTest',
					'answers', 'addAnswer', 'editAnswer'
				]},
				{name: 'ssTest.menu.specialities', state: '.specialities', checkArr: [
					'specialities', 'addSpeciality', 'editSpeciality'
				]},
				{name: 'ssTest.menu.faculties', state: '.faculties', checkArr: [
					'faculties', 'addFaculty', 'editFaculty'
				]}
			];

			$scope.isActive = function(arr) {
				for (var i = 0; i < arr.length; i++) {
					var result = $state.includes('admin.' + arr[i]);

					if (result) {
						return result;
					}
				}
			};

			$scope.exit = function() {
				ModalService.open({
					data: {
						title: "ssTest.ATTENTION",
						log: {
							logMsg: 'ssTest.menu.modalWindows.exit'
						},
						action: {
							actionTxt: "ssTest.controls.yes",
							actionFn: function(){
								logService.logOut();
							}
						}
					},
					options: {
						size: 'sm'
					}
				});
			};*/


/*.controller('ChampionshipsCtrl', ['$scope', 'logService', '$state', 'ModalService', '$cookieStore', '$cookies',
		function($scope, logService, $state, ModalService, $cookieStore, $cookies) {*/