angular
	.module('app')
	 	.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
		$urlRouterProvider.otherwise('/football-app/');
		$stateProvider
			.state('home', {
				url: '/football-app',
				templateUrl: 'home/view.html'
			})
	}]);