angular
	.module('teams')
	.controller('TeamsCtrl', ['$scope', '$http',
		function($scope, $http) {
			$http.get("http://footballbet.com.ua/api/teams/")
			.success(function (data) {
				$scope.imageTeam = 'http://footballbet.com.ua/teams/embl/';
				var teams = data.result;
				var i, j;
				var availableTeams = [];
				function getTeams() {
					for(i=0; i < teams.length; i++) {
						if(teams[i].name && teams[i].emblema) {
							availableTeams.push(teams[i]);
						}
					}
				}
				getTeams();
				$scope.teams = availableTeams;

			});
		}
]);