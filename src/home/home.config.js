angular
	.module('home')
	 	.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
		$urlRouterProvider.otherwise('/');
		$stateProvider
			.state('home.main', {
				url: '/',
				templateUrl: 'home/view-home.html'
			})
			.state('home.championships', {
				url: '/championships',
				templateUrl: 'championships/championships.html'
			})
			.state('home.teams', {
				url: '/teams',
				templateUrl: 'teams/teams.html'
			})
			.state('home.championships.teamsByChampionship', {
				url: '/teamsByChampionship/:id',
				templateUrl: 'championships/championships-teams.html'
			})

	}]);